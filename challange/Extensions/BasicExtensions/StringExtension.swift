//
//  StringExtension.swift
//  challange
//
//  Created by Neacsu Orlando on 18/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
