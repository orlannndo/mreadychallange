//
//  ViewControllerExtension.swift
//  challange
//
//  Created by Neacsu Orlando on 19/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showAlertView(title: String, message: String, buttonTitle: String = "OK", time: Double = 0,buttonHandler: @escaping (UIAlertAction) -> Void = {_ in }) {
        var elapsingTime = time
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: buttonTitle, style: .default, handler: buttonHandler)
        
        if time > 0 {
            OKAction.isEnabled = false
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
                if elapsingTime > 0 {
                    elapsingTime -= 1
                    alertController.title = "\(elapsingTime)"
                } else {
                    OKAction.isEnabled = true
                    timer.invalidate()
                }
            }
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
