//
//  HomeVCExtension.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import UIKit

extension HomeViewController {
    
    func showInitialSpinner() {
        activityIndicator.startAnimating()
        tableView.isHidden = true
        view.backgroundColor = UIColor.lightGray
    }
    
    func hideInitialSpinner() {
        activityIndicator.stopAnimating()
        view.backgroundColor = UIColor.white
        tableView.isHidden = false
    }
    
    func hideSpinners() {
        if page == 1 {
            hideInitialSpinner()
        }
        hideTableViewFooterSpinner()
    }
    
    func addSpinnerInFooterOfTableView() {
        let spinner = UIActivityIndicatorView(style: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(50))
        tableView.tableFooterView = spinner
        hideTableViewFooterSpinner()
    }
    
    func showTableViewFooterSpinner() {
        tableView.tableFooterView?.isHidden = false
    }
    
    func hideTableViewFooterSpinner() {
        tableView.tableFooterView?.isHidden = true
    }
    
    
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homecell", for: indexPath) as! HomeVCTableViewCell
        cell.idLabel.text = "\(items[indexPath.row].id!)" 
        cell.fullNameLabel.text = items[indexPath.row].fullName ?? "nill"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        performSegue(withIdentifier: "itemDetailsSegue", sender: self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height && !reachedEndOfTheTable {
            showTableViewFooterSpinner()
            reachedEndOfTheTable.toggle()
            bringDataFromGit(page: page, perPage: perPage, order: order)
        }
    }
    
}
