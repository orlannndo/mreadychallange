//
//  HomeVCTableViewCell.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import UIKit

class HomeVCTableViewCell: UITableViewCell {

    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
