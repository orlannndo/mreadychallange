//
//  ItemModel.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation
import UIKit

class Item: Codable {
    let id: Int?
    let nodeID, name, fullName: String?
    let owner: Owner?
    let itemPrivate: Bool?
    let htmlURL: String?
    let description: String?
    let fork: Bool?
    let url: String?
    let createdAt, updatedAt, pushedAt: String?
    let homepage: String?
    let size, stargazersCount, watchersCount: Int?
    let language: String?
    let forksCount, openIssuesCount: Int?
    let masterBranch, defaultBranch: String?
    let score: Double?
    
    enum CodingKeys: String, CodingKey {
        case id
        case nodeID = "node_id"
        case name
        case fullName = "full_name"
        case owner
        case itemPrivate = "private"
        case htmlURL = "html_url"
        case description, fork, url
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case pushedAt = "pushed_at"
        case homepage, size
        case stargazersCount = "stargazers_count"
        case watchersCount = "watchers_count"
        case language
        case forksCount = "forks_count"
        case openIssuesCount = "open_issues_count"
        case masterBranch = "master_branch"
        case defaultBranch = "default_branch"
        case score
    }
    
    func stringDescription() -> String {
        var result = ""
        if id != nil {
            result.append(contentsOf: "id : \(id!)\n")
        }
        
        if nodeID != nil {
            result.append(contentsOf: "nodeID : \(nodeID!)\n")
        }
        
        if name != nil {
            result.append(contentsOf: "name : \(name!)\n")
        }
        
        if fullName != nil {
            result.append(contentsOf: "fullName : \(fullName!)\n")
        }
        
        if owner != nil {
           // result.append(contentsOf: "owner : \(owner)\n")
           // too much text
        }
        
        if itemPrivate != nil {
            result.append(contentsOf: "itemPrivate : \(itemPrivate!)\n")
        }
        
        if htmlURL != nil {
            result.append(contentsOf: "htmlURL : \(htmlURL!)\n")
        }
        
        if description != nil {
            result.append(contentsOf: "description : \(description!)\n")
        }
        
        if fork != nil {
            result.append(contentsOf: "fork : \(fork!)\n")
        }
        
        if url != nil {
            result.append(contentsOf: "url : \(url!)\n")
        }
        
        if createdAt != nil {
            result.append(contentsOf: "createdAt : \(createdAt!)\n")
        }
        
        if updatedAt != nil {
            result.append(contentsOf: "updatedAt : \(updatedAt!)\n")
        }
        
        if pushedAt != nil {
            result.append(contentsOf: "pushedAt : \(pushedAt!)\n")
        }
        
        if homepage != nil {
            result.append(contentsOf: "homepage : \(homepage!)\n")
        }
        
        if size != nil {
            result.append(contentsOf: "size : \(size!)\n")
        }
        
        if stargazersCount != nil {
            result.append(contentsOf: "stargazersCount : \(stargazersCount!)\n")
        }
        
        if watchersCount != nil {
            result.append(contentsOf: "watchersCount : \(watchersCount!)\n")
        }
        
        if language != nil {
            result.append(contentsOf: "language : \(language!)\n")
        }
        if forksCount != nil {
            result.append(contentsOf: "forksCount : \(forksCount!)\n")
        }
        
        if openIssuesCount != nil {
            result.append(contentsOf: "openIssuesCount : \(openIssuesCount!)\n")
        }
        
        if masterBranch != nil {
            result.append(contentsOf: "masterBranch : \(masterBranch!)\n")
        }
        
        if defaultBranch != nil {
            result.append(contentsOf: "defaultBranch : \(defaultBranch!)\n")
        }
        
        if score != nil {
            result.append(contentsOf: "score : \(score!)")
        }
        
        return result
    }
}

