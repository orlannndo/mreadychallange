//
//  GitApiResponse.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation

class GitApiResponse: Codable {
    let totalCount: Int
    let incompleteResults: Bool
    let items: [Item]
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case incompleteResults = "incomplete_results"
        case items
    }
}

