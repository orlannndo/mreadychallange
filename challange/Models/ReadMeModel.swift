//
//  ReadMeModel.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation

class ReadMe: Codable {
    let name, path, sha: String
    let size: Int
    let url, htmlURL: String
    let gitURL: String
    let downloadURL: String
    let type, content, encoding: String
    let links: Links
    
    enum CodingKeys: String, CodingKey {
        case name, path, sha, size, url
        case htmlURL = "html_url"
        case gitURL = "git_url"
        case downloadURL = "download_url"
        case type, content, encoding
        case links = "_links"
    }
}

class Links: Codable {
    let linksSelf: String
    let git: String
    let html: String
    
    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case git, html
    }
}
