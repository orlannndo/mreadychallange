//
//  HTTPMethods.swift
//  challange
//
//  Created by Neacsu Orlando on 18/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
    case patch = "PATCH"
}
