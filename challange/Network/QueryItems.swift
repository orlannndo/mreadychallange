//
//  QueryItems.swift
//  challange
//
//  Created by Neacsu Orlando on 18/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation

enum Language: String {
    case Swift = "swift"
    case ObjectiveC = "objectivec"
}

struct GitHubInternalQuery {
    var query: String
    
    init( languages: [Language]) {
        query = "language:"
        for language in languages {
            query.append(language.rawValue)
            query.append(",")
        }
    }
    
    var queryItem: URLQueryItem {
        return URLQueryItem(name: "q", value: String(query.dropLast()))
    }
}


enum Sort: String {
    case numberOfStars = "stars"
    
    var queryItem: URLQueryItem {
        return URLQueryItem(name: "sort", value: self.rawValue)
    }
    
}

enum Order: String {
    case ascendant = "asc"
    case descendant = "desc"
    
    var queryItem: URLQueryItem {
        return URLQueryItem(name: "order", value: self.rawValue)
    }
}

struct IndexOfPage {
    var index: String
    
    init(index: Int) {
        self.index = "\(index)"
    }
    var queryItem: URLQueryItem {
        return URLQueryItem(name: "page", value: index)
    }
}

struct ItemsPerPage {
    var numberOfItems: String
    
    init(numberOfItems: Int) {
        self.numberOfItems = "\(numberOfItems)"
    }
    
    var queryItem: URLQueryItem {
        return URLQueryItem(name: "per_page", value: numberOfItems)
    }
}


