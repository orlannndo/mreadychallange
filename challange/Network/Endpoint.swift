//
//  Endpoint.swift
//  challange
//
//  Created by Neacsu Orlando on 18/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation
enum Endpoint {
    
    case searchInPublicRepositories
    case getReadme(fullName: String)
    
    var basePath: String {
        switch self {
        case .searchInPublicRepositories:
            return "https://api.github.com"
        case .getReadme( _):
            return "https://api.github.com"
        }
    }
    
    var path: String {
        switch self {
        case .searchInPublicRepositories:
            return "/search/repositories"
        case .getReadme(let fullName):
            return "/repos/\(fullName)/readme"
        }
    }
    
    var httpMethod: String {
        switch self {
        case .searchInPublicRepositories:
            return HTTPMethod.get.rawValue
        case .getReadme(_ ):
            return HTTPMethod.get.rawValue
        }
    }
    
}

