//
//  Error.swift
//  challange
//
//  Created by Neacsu Orlando on 18/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import Foundation

enum MyError: Error {
    case urlRequestBuilderFailed
    case requestFailed
    case invalidResponse(statusCode: Int)
    case rateLimitexceeded
    
    var localizedDescription: String {
        switch self {
        case .urlRequestBuilderFailed:
            return "urlRequestBuilder failed"
        case .requestFailed:
            return "request failed"
        case .invalidResponse(let statusCode):
            return "invalid response: \(statusCode)"
        case .rateLimitexceeded:
            return "API rate limit exceeded. Try again"
        }
    }
}
