//
//  Request.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//
import Foundation

class Requests {
    
    static let sharedInstance = Requests()
    
    private init() {}
    
    private func URLRequestBuilder(endpoint: Endpoint, queryItems: [URLQueryItem]) -> URLRequest? {
        var components = URLComponents(string: endpoint.basePath)
        guard components != nil else { return nil}
        components!.path = endpoint.path
        components!.queryItems = queryItems
        let url = components!.url
        guard url != nil else { return nil }
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = endpoint.httpMethod
        return urlRequest
    }
    
    private func startTask(urlRequest: URLRequest,  completionHandler: @escaping (_ data: Data?, _ response: URLResponse?, _ error: MyError?) -> Void) {
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard error == nil else {
                completionHandler(nil,nil,MyError.requestFailed)
                return
            }
            completionHandler(data,response,nil)
            }.resume()
    }
    
    func searchInPublicRepositories(languages: [Language], sortBy: Sort, orderBy: Order, page: Int, perPage: Int, completionHandler: @escaping (_ data: Data?, _ response: URLResponse?, _ error: MyError?) -> Void) {
        let indexPage = IndexOfPage(index: page)
        let itemsPerPage = ItemsPerPage(numberOfItems: perPage)
        let urlRequest = URLRequestBuilder(endpoint: .searchInPublicRepositories, queryItems: [
            GitHubInternalQuery(languages: languages).queryItem,
            sortBy.queryItem,
            orderBy.queryItem,
            indexPage.queryItem,
            itemsPerPage.queryItem
            ])
        guard urlRequest != nil else {
            completionHandler(nil, nil, MyError.urlRequestBuilderFailed)
            return
        }
        startTask(urlRequest: urlRequest!) { (data, response, error) in
            completionHandler(data,response,error)
        }
    }
    
    func getReadme(repoFullName: String, completionHandler: @escaping (_ data: Data?, _ response: URLResponse?, _ error: MyError?) -> Void) {
        let urlRequest = URLRequestBuilder(endpoint: .getReadme(fullName: repoFullName), queryItems: [])
        guard urlRequest != nil else {
            completionHandler(nil, nil, MyError.urlRequestBuilderFailed)
            return
        }
        startTask(urlRequest: urlRequest!) { (data, response, error) in
            completionHandler(data,response,error)
        }
    }
    
}












//
////  https://api.github.com/search/repositories?q=language:swift,objectivec&sort=stars&order=desc&page=1&per_page=1

//private func queryBuilder() -> String {
//    var query = "q=language:"
//    for language in languages {
//        query += language.rawValue + ","
//    }
//    return String(query.dropLast())
//}
//
//
//private func URLBuilder(endPoint: String, query: String) -> URL? {
//    let URLString = endPoint + "?" + query
//    let url = URL(string: URLString)
//    return url
//}
//
//func start(completionHandler: @escaping (GitApiResponse?, Error?) -> Void) {
//    //        let query = queryBuilder(sort: sort, order: order, languages: languages)
//    //        let url = URLBuilder(endPoint: endPoint, query: query)
//
//    URLSession.shared.dataTask(with: URL(string: "https://api.github.com/search/repositories?q=language:swift,objectivec&sort=stars&order=desc&page=\(page)&per_page=50")!) { (data, response, error) in
//        if let data = data {
//            do {
//                let response = try JSONDecoder().decode(GitApiResponse.self, from: data)
//                completionHandler(response,nil)
//            } catch let jsonErr {
//                print("Error serializing json:", jsonErr)
//                completionHandler(nil,jsonErr)
//            }
//        }
//        }.resume()
//}
//
//func cancel() {
//    URLSession.shared.invalidateAndCancel()
//}
