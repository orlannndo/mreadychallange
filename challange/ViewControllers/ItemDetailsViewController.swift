//
//  ItemDetailsViewController.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import UIKit
import WebKit

class ItemDetailsViewController: UIViewController {
    
    // MARK: Outlets
    
    
    @IBOutlet weak var repoInfoTextView: UITextView!
    @IBOutlet weak var repoReadmeWebKit: WKWebView!
    // MARK: Variables
    
    var item: Item?
    var readme : ReadMe?
    let requests = Requests.sharedInstance
    
    // MARK: Life Cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        designSetup()
        showInfo()
        if item != nil {
             getReadme(repoFullName: item!.fullName!)
        }
    }
    
    // MARK: Helpers
    
    func designSetup() {
        repoInfoTextView.layer.borderWidth = 1
        repoInfoTextView.layer.borderColor = UIColor.red.cgColor
    }
    
    func showInfo() {
        if let item = item {
            repoInfoTextView.text = item.stringDescription()
        }
    }
    
    func loadReadmeInRepoReadmeWebKit() {
        if let readme = readme {
            if let decodedString = readme.content.fromBase64()
            {
                let formatedDecodedString = decodedString.replacingOccurrences(of: "\n", with: "<br>")
                print(formatedDecodedString)
                repoReadmeWebKit.loadHTMLString(formatedDecodedString, baseURL: nil)
            }
        }
    }
    
    func getReadme(repoFullName: String) {
        requests.getReadme(repoFullName: repoFullName) { (data, response, error) in
            guard error == nil else {
                DispatchQueue.main.async {
                    self.showAlertView(title: "Error", message: error!.localizedDescription, buttonTitle: "Refresh", time: 5, buttonHandler: { (_) in
                        self.refresh()
                    })
                }
                return
            }
            
            if let response =  response as? HTTPURLResponse, let data = data {
                switch response.statusCode {
                case 200: // OK
                    do {
                        self.readme = try  JSONDecoder().decode(ReadMe.self, from: data)
                        DispatchQueue.main.async {
                            self.loadReadmeInRepoReadmeWebKit()
                        }
                    } catch (let jsonErr) {
                        self.showAlertView(title: "Error", message: jsonErr.localizedDescription)
                    }
                default: // other status codes
                    break
                }
            }
        }
    }
    
    func refresh() {
        if item != nil {
            getReadme(repoFullName: item!.fullName!)
        }
    }
    
    // MARK: Actions
    
    @IBAction func closeButtonTouchUpInside(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
