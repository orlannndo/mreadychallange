//
//  ViewController.swift
//  challange
//
//  Created by Neacsu Orlando on 15/03/2019.
//  Copyright © 2019 Neacsu Orlando. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: Variables
    
    var items: [Item] = []
    var index = 0
    var reachedEndOfTheTable = true
    var page = 1
    var perPage = 50
    var order = Order.descendant
    var requests = Requests.sharedInstance
    
    // MARK: Life cycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showInitialSpinner()
        addSpinnerInFooterOfTableView()
        bringDataFromGit(page: page, perPage: perPage, order: order)
        
    }
    
    // MARK: Helpers
    
    func bringDataFromGit(page: Int, perPage: Int, order: Order) {
        guard reachedEndOfTheTable == true else { return }
        requests.searchInPublicRepositories(languages: [.ObjectiveC, .Swift], sortBy: Sort.numberOfStars, orderBy: order , page: page, perPage: perPage) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    self.showAlertView(title: "Error", message: error.localizedDescription, buttonTitle: "Refresh", time: 5, buttonHandler: { (_) in
                        self.refresh()
                    })
                }
                return
            }
            if let response =  response as? HTTPURLResponse, let data = data {
                switch response.statusCode {
                case 200: // OK
                    do {
                        let responseFromGit = try JSONDecoder().decode(GitApiResponse.self, from: data)
                        self.items.append(contentsOf: responseFromGit.items)
                    } catch(let jsonErr) {
                        print(jsonErr)
                    }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.reachedEndOfTheTable.toggle()
                        self.hideSpinners()
                        self.page += 1
                    }
                case 403: // API rate limit exceeded. You will be able to make a new request over 30 seconds.-> https://developer.github.com/v3/#rate-limiting
                    DispatchQueue.main.async {
                        self.showAlertView(title: "Error", message: MyError.rateLimitexceeded.localizedDescription, buttonTitle: "Try again", time: 30, buttonHandler: { (_) in
                            self.refresh()
                        })
                    }
                default: // other status codes
                    DispatchQueue.main.async {
                        self.hideSpinners()
                        self.showAlertView(title: "Error", message: MyError.invalidResponse(statusCode: response.statusCode).localizedDescription)
                    }
                }
            }
        }
    }
    
    func refresh() {
        bringDataFromGit(page: page, perPage: perPage, order: order)
    }
    
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? ItemDetailsViewController {
            destinationVC.item = items[index]
        }
    }
    
    
}

